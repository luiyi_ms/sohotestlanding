<?php
/**
 * @file 
 * Implement Front controller
 */
ini_set('display_errors', 1);

/**
 * Composer Autoload
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Routing instance
 */
$router = new Core\Router();

// Add the default route
$router->add('', ['controller' => 'Home', 'action' => 'index']);

//launch process method for find the route
try {
   
   $router->process($_SERVER['QUERY_STRING']);

} catch (Exception $e) {
    print "¡Route not found! " . $e;
}
