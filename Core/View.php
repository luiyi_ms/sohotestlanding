<?php
/**
 * @file 
 * Simple base class for render twig templates
 */

namespace Core;

/**
 * View
 *
 */
class View
{


    /**
     * Render a view template using Twig
     *
     * @param string $template  The template file
     * @param array $args  Associative array of data to display in the view (optional)
     *
     * @return void, print theme render in twig
     */
    public static function renderTemplate($template, $args = [])
    {
        static $twig = null;
        
        if ($twig === null) {
            $loader = new \Twig\Loader\FilesystemLoader(dirname(__DIR__) . '/App/Views');
            $twig = new \Twig\Environment($loader);
            $twig->addExtension(new \Twig\Extension\DebugExtension());
        }

        echo $twig->render($template, array('data'=>$args));
    }
}
