<?php
/**
 * @file
 * Contains App\Controllers\Home.
 */
 
namespace App\Controllers;

use \Core\View;
use \App\Models\Projects;

/**
 * Index Home controller
 *
 */
class Home extends \Core\Controller
{

    /**
     * home landing page
     *
     * @return void
     */
    public function indexAction()
    {
    	$projects = Projects::getAll();
    		
        View::renderTemplate('Home/index.html', $projects);
    }
}