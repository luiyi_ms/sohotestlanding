<?php
/**
 * @file 
 * define base class for project entity database schema
 */

namespace App\Models;

use PDO;

/**
 * Projects model
 *
 */
class Projects extends \Core\Model
{
    /**
     * Get all the projects as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT title,description,link FROM projects');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
