-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 10, 2020 at 04:19 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projects_soho`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `link` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `description`, `link`) VALUES
(1, 'Akt Motos', 'Portal empresarial para mostrar catalogo de motos y los diferentes servicios relacionados con el soporte y mantenimiento, la comunidad y noticias de la empresa.', 'https://www.aktmotos.com/'),
(2, 'Kalley', 'Proyecto de catalogo de productos de electrodomesticos de la marca Kalley, asi como las ultimas noticias y promociones relacionadas con la marca', 'https://www.kalley.com.co/'),
(3, 'Portal CaracolPlay', 'Portal de streaming de video por suscripcion, en el cual se pueden ver las ultimas producciones televisivas del canal caracol, asi como todas las campañas publicitarias relacionadas con la marca.', 'https://play.caracoltv.com/'),
(4, 'Portal COlombia Travel', 'Portal web enfocado en promocionar todos los destinos turisticos de Colombia, asi como todas las noticias relacionadas con el sector turismo.', 'https://colombia.travel/es');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
