# Simple PHP MVC Testing for soho aplication

## Description app

1. Architect MVC
2. Integration the Twig para template page.
3. Use PDO conection Database
4. Include Boostrap 4 for mockup
5. Include PSR-4 for autoloading class with composer
6. Integration Sass preprocess for styles css


Installation
- restore Mysql database dump file "projects_soho.sql"
- execute "composer install"
- execute command "composer dump-autoload"

## Configuration

Configuration settings are stored in the [App/Config.php](App/Config.php) class. Default settings include database connection data.

## Routing

Routes are added in the [front controller](public/index.php). Home route is included that routes to the `index` action in the [Home controller](App/Controllers/Home.php).
